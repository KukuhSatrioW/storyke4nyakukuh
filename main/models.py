from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class modelMatkul(models.Model):
    mataKuliah = models.CharField(max_length = 100)
    dosenPengajar = models.CharField(max_length = 100)
    jumlahSKS = models.IntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(6)])
    deskripsi = models.TextField(blank=True , null=True)
    semester = models.CharField(max_length = 30)
    ruangan = models.CharField(max_length = 100)
