$(document).ready(function(){
    $("button").click(function(){
        var titleSearch = $("#search").val();
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + titleSearch,
            dataType: "json",


            success: function (data) {
                console.log(data);

                var tbody = $(".result");
                tbody.empty();
                for (i = 0; i < data.items.length; i++) {
                    tbody.append("<tr>");

                    try {
                        var cover = data.items[i].volumeInfo.imageLinks.smallThumbnail;
                        var title = data.items[i].volumeInfo.title;
                        var author = data.items[i].volumeInfo.authors[0];
                        var description = data.items[i].volumeInfo.description;
                    } catch (error) {
                        var cover = "https://i.ibb.co/McfrGBF/noImage.jpg";
                        var author = "No Author";
                    }

                    tbody.append("<th scope='row' style='font-size: 15px;'>" + (i + 1) + "</th>");
                    tbody.append("<td>" + "<img class='img-fluid' src=" + cover + ">" + "</td>")
                    tbody.append("<td style='font-size: 15px;'>" + title + "</td>" + "<td style='font-size: 15px;'>" + author + "</td>" + "<td style='font-size: 15px;'>" + description + "</td>");

                    tbody.append("</tr>");
                }
            }
        })
    })
})
