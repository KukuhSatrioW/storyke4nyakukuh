from django import forms
from .models import modelMatkul

class formMatkul(forms.ModelForm):
	class Meta:
		model = modelMatkul
		fields = '__all__'
