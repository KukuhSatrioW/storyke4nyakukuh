from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('story5/', views.story5, name='story5'),
    path('story5/detailmatkul/<int:id>/', views.detail, name='detailmatkul'),
    path('story5/deletematkul/<int:id>/', views.delete, name='deletematkul'),
    path('story7/', views.story7, name='story7'),
    path('story8/', views.story8, name='story8'),
    path('user/', views.story9,name='story9'),
    path('user/login/', views.login_user,name='login'),
    path('user/logout/', views.logout_user,name='logout'),
    path('user/signup/', views.signup_user,name='signup')

]
