from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from .forms import formMatkul
from .models import modelMatkul
from django.http import HttpResponseRedirect
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

def home(request):
    return render(request, 'main/home.html')

def story5(request):
    formKukuh = formMatkul()

    if request.method=='POST':
        formKukuh = formMatkul(request.POST)
        if formKukuh.is_valid():
            formKukuh.save()
            return redirect('/story5')

    previewTable = modelMatkul.objects.all()
    dataKukuh = {"createMatkul" : formKukuh, "tampilkanTable" : previewTable}
    return render(request, 'main/Story5.html', dataKukuh)

def detail(request, id):
    matkul = get_object_or_404(modelMatkul, id=id)
    detailMatkul = {"createDetail" : matkul}
    return render(request, 'main/Details.html', detailMatkul)

def delete(request, id):
    delete = modelMatkul.objects.filter(id=id)
    delete.delete()
    return HttpResponseRedirect('/story5')

def story7(request):
    return render(request, 'main/story7.html')

def story8(request):
    return render(request, 'main/story8.html')

def story9(request):
    return render(request,'main/user.html')

def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        print("{}: {} ".format(username,password))
        if not username or not password:
            messages.error(request, "Please enter username / password correctly")
            return HttpResponseRedirect('/user/login')

        user = authenticate(request,username = username,password = password)
        if user is not None:
            login(request,user)
            return HttpResponseRedirect('/user/')
        else:
            messages.error(request, "Wrong credentials (Wrong Username / Password)")
            return HttpResponseRedirect('/user/login')
    else:
        return render(request,'main/login.html')

def logout_user(request):
    logout(request)
    messages.success(request, "Successfully logged out.")
    return HttpResponseRedirect('/user/')

def signup_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        if not username or not password:
            messages.error(request, "Please enter username / password correctly")
            return HttpResponseRedirect('/user/signup')
        if User.objects.filter(username__iexact = username).exists():
            messages.error(request, "Username taken")
            return HttpResponseRedirect('/user/signup')
        else:
            user = User.objects.create_user(username=username, password=password)
            user.save()
            return HttpResponseRedirect('/user/login')
    else:
        return render(request,'main/signup.html')
