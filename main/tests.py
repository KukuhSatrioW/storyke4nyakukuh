from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from selenium import webdriver
from main.views import *
import unittest

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

class ViewsTest(TestCase):
    def test_story9_url(self):
        response = Client().get('/user/')
        self.assertEqual(response.status_code,200)

    def test_login_url(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code,200)
    
    def test_logout_url(self):
        response = Client().get('/user/logout/')
        self.assertEqual(response.status_code,302)

    def test_signup_url(self):
        response = Client().get('/user/signup/')
        self.assertEqual(response.status_code,200)

    def test_story9_func(self):
        found = resolve('/user/')
        self.assertEqual(found.func,story9)

    def test_login_func(self):
        found = resolve('/user/login/')
        self.assertEqual(found.func,login_user)

    def test_signup_func(self):
        found = resolve('/user/signup/')
        self.assertEqual(found.func,signup_user)

    def test_templ_story9(self):
        response = Client().get('/user/')
        self.assertTemplateUsed(response,'main/user.html')

    def test_templ_login(self):
        response = Client().get('/user/login/')
        self.assertTemplateUsed(response,'main/login.html')
    
    def test_templ_signup(self):
        response = Client().get('/user/signup/')
        self.assertTemplateUsed(response,'main/signup.html')